package com.example.studentapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {
    companion object {
        var name_key = "name_key"
        var gender_key = "gender_key"
        var sport_key = "sport_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        save_button.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)

            intent.putExtra(name_key, name_text.text.toString())

            when {
                male_radio.isChecked -> {
                    intent.putExtra(gender_key, "Male")
                }
                female_radio.isChecked -> {
                    intent.putExtra(gender_key, "Female")
                }
                else -> {
                    intent.putExtra(gender_key, "NaN")
                }
            }

            val sports = ArrayList<String>()

            if (basketball_button.isChecked)
                sports.add("Basketball")
            if (football_button.isChecked)
                sports.add("Football")
            if (baseball_button.isChecked)
                sports.add("Baseball")
            intent.putExtra(sport_key, sports)

            startActivity(intent)
        }
    }
}